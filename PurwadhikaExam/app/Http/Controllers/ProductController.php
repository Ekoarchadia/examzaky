<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    function getUnit(){
        //menggunakan Query Builder

        $unit = DB:table('unit')->get();

        return view('unit', ["rumah" => $unit]);
    }

    function createUnit(Request $request){
        DB::beginTransaction();
        //menggunakan metode Eloquent ORM

        try{
            $newUnit = new unit;
            $newUnit->kavling = $request->input('kavling');
            $newUnit->kavling = $request->input('block');
            $newUnit->kavling = $request->input('no_rumah');
            $newUnit->kavling = $request->input('harga_rumah');
            $newUnit->kavling = $request->input('luas_tanah');
            $newUnit->kavling = $request->input('luas_bangunan');
            $newUnit->save();

            DB::commit();
            return response()->json(["message"=>"success"], 200);
        }catch(\Exception $e){
            return response()->json(["message"=> $e->getMessage], 500);
        }
    }

    function deleteUnit($id){
        //menggunakan metode RAW SQL Query
        DB::delete('delete from unit where id = ?', [$id]);
        return redirect("/");
    }
}
